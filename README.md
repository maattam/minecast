Minecast
================

## Minecraft computecraft irc-like chat platform

Minecast is an irc-like chat platform that allows communicating with other players by alternative
means to the good old ingame chat.

Minecast uses a client-server model where a single computecraft computer with a redstone modem acts
as a server that other computers running minecast client can communicate with.

Minecast supports basic chat functionality; message logging, nicknames, timeout detection and quitting.
It also has an irssi-like user interface!

Author
------
Matti Määttä ([maatta.matti@gmail.com])

[maatta.matti@gmail.com]: mailto:maatta.matti@gmail.com

[![screenshot](https://bitbucket.org/maattam/minecast/raw/941c0a7aff878cd535f1e5e8bf45cc7988395657/minecast.jpg)](https://bitbucket.org/maattam/minecast/raw/941c0a7aff878cd535f1e5e8bf45cc7988395657/minecast.jpg)
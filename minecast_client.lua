-- minecast client 1.0
-- Author: Matti Määttä

local version = "minecast 1.1 alpha"

-- config
local timeout = 30
local monitorSide = "right"
local modemSide = "back"
local timestamps = true

local arg = {...}
local input = ""
local lines = {}
local server = nil
local ping = false
local maxlines = 0
local refresh = false
local timeoutTimer = nil
local contimer = nil

local msghandler = {}

function token(str, char, begin)
	_, pos = string.find(str.."", char, begin, true)
	if pos == nil then
		return string.sub(str, begin), nil
	else
		return string.sub(str, begin, pos-1), pos+1
	end
end

function timestamp(time)
	if time ~= nil then
		return "[" .. time .. "]"
	else
		return "[" .. textutils.formatTime(os.time(), true) .. "]"
	end
end

function addLine(str, color, time)
	if #lines >= maxlines then
		table.remove(lines, 1)
	end
	if timestamps then str = timestamp(time).." "..str end
	lines[#lines+1] = {["str"] = str, ["color"] = color}
	refresh = true
end

function drawStatusBar(target)
	width, height = target.getSize()
	time = textutils.formatTime(os.time(), true)
	status = "offline"
	if server ~= nil then status = "online" end

	target.setCursorPos(1, 1)
	if target.isColor() then
		target.setBackgroundColor(colors.blue)
		target.setTextColor(colors.white)
	end
	
	target.write(time..string.rep(" ", width-#time-#status)..status)
	if not target.isColor() then
		target.setCursorPos(1, 2)
		target.write(string.rep("-", width))
	else
		target.setBackgroundColor(colors.black)
	end
	
	-- return cursor
	target.setCursorPos(#input+3, height)
	target.setCursorBlink(true)
end

function drawInputBar(target)
	width, height = target.getSize()
	target.setCursorPos(1, height)
	if target.isColor() then
		target.setBackgroundColor(colors.blue)
		target.setTextColor(colors.white)
		target.write("> "..input)
		if width-2-#input > 0 then target.write(string.rep(" ", width-2-#input)) end
		target.setBackgroundColor(colors.black)
	else
		target.write("> "..input)
	end
	
	target.setCursorPos(#input+3, height)
	target.setCursorBlink(true)
end

function drawUi(target)
	width, height = target.getSize()
	target.clear()
	
	if target.isColor() then
		target.setBackgroundColor(colors.black)
		target.setCursorPos(1, height-#lines)
	else
		target.setCursorPos(1, height+1-#lines)
	end
	
	-- print lines
	-- hackfix
	if target ~= term then term.redirect(target) end
	for i, v in ipairs(lines) do
		if target.isColor() then
			if v.color == nil then
				target.setTextColor(colors.white)
			else
				target.setTextColor(v.color)
			end
		end
		print(v.str)
	end
	if target ~= term then term.restore() end
	
	drawStatusBar(target)
	drawInputBar(target)
end

function handleKey(key)
	if key == 28 then -- enter
		handleInput()
	elseif key == 14 then -- backspace
		input = string.sub(input, 1, #input-1)
	end
end

-- data: 	msg [message:string]
--			cmd [args:string]
function handleInput()
	if not input or input == "" then
		return
	end
	str = input
	input = ""
	
	if server == nil then
		addLine("not connected to server", colors.red)
		return
	end
	
	if string.sub(str, 1, 1) == "/" and #str > 2 then -- command
		rednet.send(server, "cmd "..string.sub(str, 2))
		
		-- local quit handler
		if token(str, " ", 1) == "/quit" then
			rednet.close(modemSide)
			error()
		end
	else
		rednet.send(server, "msg "..str)
	end
end

function connect()
	if server == nil then
		ping = false
		addLine("connecting to server " .. arg[1] .. "..", colors.yellow)
		rednet.broadcast("hello "..arg[1])
		contimer = os.startTimer(15) -- check again
	end
end

function checkTimeout()
	if not ping then -- timeout
		server = nil
		addLine("connection timed out", colors.red)
		connect()
	else
		ping = false
		timeoutTimer = os.startTimer(timeout)
	end
end

function handleMessage(event)
	cmd, pos = token(event[3], " ", 1)
	if msghandler[cmd] ~= nil then
		args = ""
		if pos ~= nil then
			args = string.sub(event[3], pos)
		end
		msghandler[cmd](event[2], args, event[4])
	else
		addLine("unknown command: "..cmd, colors.red)
	end
end

-- data:	msg [id] [nick] [message]
msghandler["msg"] = function(sender, message, ...)
	if sender ~= server then
		return
	end
	id, pos = token(message, " ", 1)
	nick, pos = token(message, " ", pos)
	message = string.sub(message, pos)
	
	str = string.format("%s:%s> %s", id, nick, message)
	if nick == "op" then
		addLine(str, colors.red)
	else
		addLine(str)
	end
end

-- data:	ping
msghandler["ping"] = function(sender, ...)
	if server == sender then
		rednet.send(server, "pong")
		ping = true
	end
end

-- data:	hello [id]
msghandler["hello"] = function(sender, message, distance)
	if server ~= nil or tonumber(message) ~= os.getComputerID() then
		return
	end
	server = sender
	ping = true
	addLine(string.format("connected to %s (%d), distance: %d", arg[1], sender, distance),
		colors.lime)
	
	timeoutTimer = os.startTimer(timeout)	-- start timeout
end

-- data:	log [time] [id] [nick] [message]
msghandler["log"] = function(sender, message, ...)
	tstr, pos = token(message, " ", 1)
	id, pos = token(message, " ", pos)
	nick, pos = token(message, " ", pos)
	message = string.sub(message, pos)
	
	addLine(string.format("%s:%s> %s", id, nick, message), colors.lightGray, tstr)
end

-- init
local monitor = peripheral.wrap(monitorSide)
local _, theight = term.getSize()
if monitor ~= nil then
	local _, mheight = monitor.getSize()

	if theight > mheight then maxlines = theight-2
	else maxlines = mheight-2 end
else
	maxlines = theight-2
end

addLine(version, colors.lime)
addLine("server name: " .. arg[1], colors.yellow)
rednet.open(modemSide)
connect()
drawUi(term)
if monitor ~= nil then drawUi(monitor) end

local clock = os.startTimer(3)

while true do
	event = {os.pullEvent()}
	
	if event[1] == "char" then
		input = input .. event[2]
		drawInputBar(term)
		if monitor ~= nil then drawInputBar(monitor) end
	elseif event[1] == "key" then
		handleKey(event[2])
		drawInputBar(term)
		if monitor ~= nil then drawInputBar(monitor) end
	elseif event[1] == "rednet_message" then
		handleMessage(event)
	elseif event[1] == "timer" then
		if event[2] == clock then
			clock = os.startTimer(3) -- update clock
			drawStatusBar(term)
			if monitor ~= nil then drawStatusBar(monitor) end
			
		elseif event[2] == contimer then connect()
		elseif event[2] == timeoutTimer then checkTimeout()
		end
	end
	
	if refresh then
		drawUi(term)
		if monitor ~= nil then drawUi(monitor) end
		refresh = false
	end
end
		

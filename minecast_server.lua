-- minecast server 1.0
-- Author: Matti M��tt�

local version = "minecast server 1.1 alpha"

-- constants
local timeout = 15
local loglen = 10

local arg = {...}
local backlog = {}

local msghandler = {}
local cmdhandler = {}

-- users[id] = {nick, connected, ping}
local users = {}

function token(str, char, begin)
	_, pos = string.find(str.."", char, begin, true)
	if pos == nil then
		return string.sub(str, begin), nil
	else
		return string.sub(str, begin, pos-1), pos+1
	end
end

function timestamp(time)
	if time ~= nil then
		return "[" .. time .. "]"
	else
		return "[" .. textutils.formatTime(os.time(), true) .. "]"
	end
end

function log(sender, message)
	print(string.format("%s %d: %s", timestamp(), sender, message))
end

function addBackLog(id, nick, message)
	if #backlog > loglen then
		table.remove(backlog, 1)
	end
	time = textutils.formatTime(os.time(), true)
	backlog[#backlog+1] = string.format("%s %d %s %s", time, id, nick, message)
end

function saveAll()
	-- save users
	handle = fs.open("users", "w")
	for id, user in pairs(users) do
		if user.nick ~= nil then
			handle.writeLine(id.." "..user.nick)
		end
	end
	handle.close()
	
	-- save backlog
	handle = fs.open("backlog", "w")
	for i, v in ipairs(backlog) do
		handle.writeLine(v)
	end
	handle.close()
end

function loadAll()
	-- load users
	handle = fs.open("users", "r")
	num = 0
	if handle ~= nil then
		while true do
			line = handle.readLine()
			if line == nil then break end
		
			id, pos = token(line, " ", 1)
			nick = string.sub(line, pos)
			users[tonumber(id)] = {["nick"] = nick,
				["connected"] = false, ["ping"] = false}
			num = num + 1
		end
		handle.close()
	end
	
	-- load backlog
	handle = fs.open("backlog", "r")
	if handle ~= nil then
		while true do
			line = handle.readLine()
			if line == nil then break end
			backlog[#backlog+1] = line
		end
		handle.close()
	end
	
	print("loaded "..num.." users")
	print("loaded "..#backlog.." log entries")
end

function sendAll(message)
	for id, user in pairs(users) do
		if user.connected then
			rednet.send(id, message)
		end
	end
end

function pingAll()
	-- check timeout
	for id, user in pairs(users) do
		if user.connected then
			if not user.ping then
				user.connected = false
				if user.nick ~= nil then
					opmessage(nil, user.nick.." has quit, reason: timeout")
				end
			else
				user.ping = false
			end
		end
	end
	
	sendAll("ping")
end

function handleMessage(event)
	if event[3] ~= "pong" then log(event[2], event[3]) end
	cmd, pos = token(event[3], " ", 1)
	if msghandler[cmd] ~= nil then
		args = ""
		if pos ~= nil then
			args = string.sub(event[3], pos)
		end
		msghandler[cmd](event[2], args, event[4])
	end
end

function opmessage(target, message)
	if target == nil then
		sendAll("msg @ op " .. message)
	elseif users[target] ~= nil and users[target].connected then
		rednet.send(target, "msg @ op " .. message)
	end
end

-- message handlers
-- msg [message]
msghandler["msg"] = function(sender, message, distance)
	user = users[sender]
	if user == nil then return end
	if user.nick == nil then
		opmessage(sender, "No nick set! Try /nick")
	else
		addBackLog(sender, user.nick, message)
		sendAll(string.format("msg %d %s %s", sender, user.nick, message))
	end
end

-- cmd [args]
msghandler["cmd"] = function(sender, args, distance)
	if users[sender] == nil or not users[sender].connected then
		return
	end
	
	cmd, pos = token(args, " ", 1)
	if cmdhandler[cmd] ~= nil then
		params = ""
		if pos ~= nil then
			params = string.sub(args, pos)
		end
		cmdhandler[cmd](sender, params, distance)
	end
end

-- pong
msghandler["pong"] = function(sender, ...)
	if users[sender] ~= nil and users[sender].connected then
		users[sender].ping = true
	end
end

-- hello [server]
msghandler["hello"] = function(sender, server, distance)
	if server == arg[2] then
		if users[sender] == nil then
			users[sender] = {["nick"] = nil, ["connected"] = true, ["ping"] = true}
		else
			users[sender].connected = true
			users[sender].ping = true
			if users[sender].nick ~= nil then
				opmessage(nil, users[sender].nick .. " has connected")
			end
		end
		
		print(sender.." connected")
		rednet.send(sender, "hello "..sender)
		
		-- send backlog
		for i, v in ipairs(backlog) do
			rednet.send(sender, "log "..v)
		end
	end
end

function checkNick(nick)
	-- sanity check
	if nick == nil or #nick < 3 or #nick > 6 or
		string.find(nick, " ", 1) ~= nil then
		return false
	end
	
	-- already in use?
	for id, user in pairs(users) do
		if user.nick == nick then
			return false
		end
	end
	
	return true
end

-- command handlers
-- nick [nick]
cmdhandler["nick"] = function(sender, nick, ...)
	user = users[sender]
	if user ~= nil and user.connected then
		if checkNick(nick) then
			if user.nick == nil then
				opmessage(nil, nick .. " has connected")
			else
				opmessage(nil, user.nick .. " is now known as " .. nick)
			end
			
			user.nick = nick
		else
			opmessage(sender, "Invalid nick: " .. nick)
		end
	end
end

cmdhandler["quit"] = function(sender, message, ...)
	user = users[sender]
	if user ~= nil and user.connected then
		user.connected = false
		if user.nick ~= nil then
			opmessage(nil, user.nick.." has quit, reason: "..message)
		end
	end
end

print(version)
rednet.open(arg[1])
loadAll()
-- start ping/pong
local timeo = os.startTimer(timeout)
-- start state saver
local save = os.startTimer(30)

while true do
	event = {os.pullEvent()}
	
	if event[1] == "rednet_message" then
			handleMessage(event)
	elseif event[1] == "timer" then
		if event[2] == timeo then
			timeo = os.startTimer(timeout)
			pingAll()
		elseif event[2] == save then
			save = os.startTimer(30)
			saveAll()
		end
	end
end